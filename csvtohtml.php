<!DOCTYPE html>
<html>
<head>
    <style> 
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
    <?php
    //converte arquivo csv em tabela html
    //O arquivo .csv a ser aberto deve estar no mesmo diretório deste script
    $numLinha=1;
    $posicao=-1;
    if (($arquivo = fopen("nome_do_arquivo.csv", "r")) !== FALSE) { //arquivo aberto no modo leitura
        echo"<table>";
            while (($linha = fgetcsv($arquivo, 1000, "^")) !== FALSE) {//neste caso o delimitador dos campos no arquivo csv é "^"
                                                                       //no caso de usar outro delimitador substituir o ^ por outro, por exemplo ";" ou ","
                                                                     //limite para cada linha do CSV 1000 caracteres
                echo"<tr>";
                    $colunas= count($linha);
                    if($numLinha==1) {
                        $posicao = buscaDadoRestrito("CPF", $linha);//chama função para verificar se há dado que não pode ser exibido ( CPF)
                    }
                    for ($i=0; $i < $colunas; $i++) {

                        if($numLinha==1 && $posicao < 0){
                            echo "<th>". $linha[$i] . "</th>";//A primeira linha do arquivo csv será a primeira da tabela
                                                //A primeira linha da tabela pode ser tratada com CSS formatando a tag th
                        }
                        elseif ($numLinha==1 && $posicao >= 0 && $posicao != $i){//Não imprime coluna restrita
                            echo "<th>". $linha[$i] . "</th>";
                        }
                        elseif($numLinha >1 && $posicao < 0){
                            echo "<td>". $linha[$i] . "</td>";//Linha após a primeira da tabela pode ser tratada com CSS formatando a tag td
                        }
                        elseif ($numLinha >1 && $posicao >= 0 && $posicao != $i){//Não imprime coluna restrita
                            echo "<td>". $linha[$i] . "</td>";
                        }
                    }
                    $numLinha++;
                echo"</tr>";
            }
        echo"</table>";
        fclose($arquivo);
    }

    function buscaDadoRestrito($dado,$listaDeDados){//verifica se no arquivo csv consta cpf em alguma coluna e retorna a posicao
        $posicaoNaLista=-1;
        $qtdCamposLista=count($listaDeDados);
        for ($x =0; $x < $qtdCamposLista; $x++)
        {
            $string=strtoupper($listaDeDados[$x]);
            if(substr_count($string,$dado) > 0){
                $posicaoNaLista=$x;
            }

        }
        return $posicaoNaLista;
    }
    ?>
</body>
</html>

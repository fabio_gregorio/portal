# Conversor de arquivo .csv em tabela html



## Sobre

Trata-se de um script em PHP 7 para conversão de um arquivo .csv em tabela html

## Instalação

- O script contido neste [link](csvtohtml.php), desenvolvido em PHP 7, deve estar contido no mesmo diretório do arquivo .csv a ser convertido, no caso do arquivo encontrar-se em diretório diferente ajustar o caminho do arquivo no argumento da função "fopen()".

```
if (($arquivo = fopen("nome_do_arquivo.csv", "r")) !== FALSE) 
```
- Para facilitar a inclusão  deste conversor numa página PHP já existente, prefira a utilização da include contida neste [link](csvtohtml.inc) ao invés de utilizar o arquivo csvtohtml.php, salve a include "csvtohtml.inc" e o arquivo .csv no mesmo diretório dá página PHP existente, num local adequado dentro do código da página PHP insira o código abaixo.
```
<?php include "csvtohtml.inc"; ?>

```
 O script foi testado utilizando o servidor web Apache 2.4

## Configurações

Inserir o nome do arquivo csv como argumento da função "fopen()"
```
if (($arquivo = fopen("nome_do_arquivo.csv", "r")) !== FALSE) 
```
Para efeito de desempenho, a quantidade de caracteres a ser lida em cada linha do arquivo .csv foi limitada em 1000 caracteres na função "fgetcsv()" mas o ajuste deve ser feito conforme necessidade.
O delimitador de campos do arquivo csv deve ser configurado na função "fgetcsv()", no exemplo apresentado o delimitador é "^" mas pode ser substituído por ";" ou "," 

```
   while (($linha = fgetcsv($arquivo, 1000, "^")) !== FALSE)
```
A primeira linha do arquivo .csv será tratada como cabeçalho da tabela html

```
                    for ($i=0; $i < $coluna; $i++) {
                        if($numLinha==1){
                            echo "<th>". $linha[$i] . "</th>";

```
O script contém uma restrição para que caso haja uma coluna do arquivo .csv contendo a informação de CPF a mesma não seja exibida na tabela HTML

```
$posicao = buscaDadoRestrito("CPF", $linha);//chama função para verificar se há dado que não pode ser exibido ( CPF)

```
## Formatação

A formatação da tabela pode ser modificada alterando-se o código CSS conforme necessidade

```
    <style> 
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>

```
## Autor
Fabio Gregorio dasgregorio@gmail.com

